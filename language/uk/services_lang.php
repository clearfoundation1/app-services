<?php

$lang['services_app_description'] = 'Додаток «Сервіси» використовується для керування системними демонами та їх станом роботи та надає корисний огляд. Ви також можете контролювати, які служби запускаються під час завантаження.';
$lang['services_app_name'] = 'Сервіси';
$lang['services_boot_status'] = 'Запустить';
$lang['services_description'] = 'Опис cервісу';
$lang['services_service'] = 'Сервіс';
$lang['services_services'] = 'Сервіси';
